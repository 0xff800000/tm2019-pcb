# Softcore ADC PCB design

## CMOD A7 dimensions

+ The Cmod A7 has a 48-pin DIP connector for connecting to breadboards and custom fixtures. The pins have 100-mil spacing, and the entire module is .7 inches by 2.75 inches.

## Layers

+ Layer prupose
	* TOP : Input signal, SPI lines
	* Layer 2 : GND
	* Layer 3 : Power (split in two : 5V under connector and 3.3V around DAC)
	* Bottom : DAC output, misc

+ Layer spacing : 10 mils, 42.5 mils, 10 mils

# Compute transmission lines characteristic impedance

+ [Coplanar Waveguide calculator](http://wcalc.sourceforge.net/cgi-bin/coplanar.cgi?wc_unit_menu_1_0=nH&wc_unit_menu_1_1=inch&wc_unit_menu_2_0=mOhm&wc_unit_menu_2_1=inch&wc_unit_menu_3_0=pF&wc_unit_menu_3_1=inch&wc_unit_menu_4_0=uMho&wc_unit_menu_4_1=inch&wc_unit_menu_7_0=dB&wc_unit_menu_8_0=dB&wc_unit_menu_8_1=inch&wc_unit_menu_12_0=mil&wc_unit_menu_11_0=ns&wc_unit_menu_13_0=inch&w=16.8097&wc_unit_menu_0_0=mil&synth_w=calc&s=10&l=141.487&wc_unit_menu_5_0=mil&tmet=1.35&rho=3e-08&wc_unit_menu_9_0=Ohm&wc_unit_menu_9_1=m&rough=0.001&wc_unit_menu_10_0=mil&h=10&es=4.8&tand=0.01&freq=900&wc_unit_menu_6_0=MHz&withgnd=on&Ro=50&elen=7.17676)
